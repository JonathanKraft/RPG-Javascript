const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
  mode: 'development',
  entry: {
    creat_char: './src/js/create-character.js',
    index : './src/js/index.js'
  },
  output : {
      filename: './[name].bundle.js'
  },
  devtool: 'inline-source-map'
};
