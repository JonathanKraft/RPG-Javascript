
import skillList from '../json/skills.json'
import monsters from "../json/jobAttributs.json"

export class Character {
/**
 * This class defines a Character. It takes different stats as parameters.
 * Different methods are used to interact with other Characters or with itself.
 * Theses methods manage the level up part, the stats growth and the profession choice.
 * maxHealth and maxMana stores the maximum amount of theses value so that heals don't overcap this stat.
 * @param {string} name 
 * @param {string} profession 
 * @param {number} health 
 * @param {number} mana 
 * @param {number} strength 
 * @param {number} vitality 
 * @param {number} magic 
 */
  constructor (name, profession, health, mana, strength, vitality, magic, exp = 0, level = 0){
    this.name = name;
    this.profession = profession;
    this.health = health;
    this.maxHealth = health;
    this.mana = mana;
    this.maxMana = mana;
    this.strength = strength;
    this.vitality = vitality;
    this.magic = magic;
    this.exp = exp;
    this.level = level;

    this.alive = true;

    this.ability1=skillList[profession][0].name;
    this.ability2=skillList[profession][1].name;
    this.ability3=skillList[profession][2].name;
    this.ability4="Heal";
  }

  /**
   * 
   * Takes in a value and adds it to the stat 
   */
  upgradeStrength(value){
    this.strength = this.strength + value
  }
  upgradeVitality(value){
    this.vitality = this.vitality + value
  }
  upgradeMagic(value){
    this.magic = this.magic + value
  }
  upgradeHealth(value){
    this.health = this.health + value
    this.maxHealth = this.health;
  }
  upgradeMana(value){
    this.mana = this.mana + value
    this.maxMana = this.mana;
  }

  healHealth(value){
    this.health += value;
    this.manageHealth();
  }
  healMana(value){
    this.mana += value;
    this.manageMana();
  }
  // Prevents health and mana to have values superior to max value
  manageHealth(){
    if (this.health > this.maxHealth){
      this.health = this.maxHealth;
    }
  }
  manageMana(){
    if (this.mana > this.maxMana){
      this.mana = this.maxMana;
    }
  }

  /**
   * 
   * Experience and level up.
   * Experience goes from 0 to 100, when hit hits 100 or more, exp recieves exp -100 and the   character gains a level (levelup)
   */
  gainExp (experiencePoints){
    this.exp = this.exp + experiencePoints;
    if (this.exp === 100){
      this.level = this.level++
      this.levelUp();
    }
    else if (this.exp > 100){
      this.levelUp();
      this.exp = this.exp - 100;
    }
  }
  levelUp (){       // HERE PUT ALL PROFESSION AND STATS GROWTH maxD ON THEM
    this.level++;
    if (this.profession === "Wizard"){
      this.upgradeStrength(1);
      this.upgradeVitality(2);
      this.upgradeMagic(5);
      this.upgradeHealth(10);
      this.upgradeMana(20);
    }
    else if (this.profession === "Fighter"){
      this.upgradeStrength(4);
      this.upgradeVitality(2);
      this.upgradeMagic(2);
      this.upgradeHealth(20);
      this.upgradeMana(10);
    }
    else if (this.profession === "Thief"){
      this.upgradeStrength(5);
      this.upgradeVitality(1);
      this.upgradeMagic(2);
      this.upgradeHealth(15);
      this.upgradeMana(15);
    }
    else if (this.profession === "Paladin"){
      this.upgradeStrength(3);
      this.upgradeVitality(4);
      this.upgradeMagic(2);
      this.upgradeHealth(16);
      this.upgradeMana(14);
    }
  }

  /**
   * strength and magic stats are used to deal damages to enemies
   */
  dealPhysicalDmg(){
    return this.strength;
  }
  dealMagicDmg(){
    return this.magic;
  }

  /**
   *  Methods to take in damages and check is the character is dead
   */
  takeDamage(value){
    this.health -= value;
  }

  isHeDead (){
    if (this.health <= 0){
      this.alive = false;
    }
  }
}