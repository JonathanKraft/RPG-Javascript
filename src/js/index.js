//import en mode webpack

import { Skill } from "./skill";
import { Character } from "./character";
import { Toolbox } from "./toolBox";
import './jobSkills';
import monsters from "../json/jobAttributs.json";




//test de création d'un perso 
let tools = new Toolbox();
let player = JSON.parse(localStorage.getItem('mainCharacter'))
player = new Character(player.name, player.profession, player.health, player.mana, player.strength, player.vitality, player.magic);

let choiceEnemy = Math.round(Math.random() * (2 - 0) + 0);
let choiceEnemyJob = ["Mecabird", "Ghost", "Little Mage"];

let selectedEnemyJob = choiceEnemyJob[choiceEnemy];


//creating an enemy

let enemy = new Character(monsters[selectedEnemyJob][0].name, selectedEnemyJob, monsters[selectedEnemyJob][0].health, monsters[selectedEnemyJob][0].mana, monsters[selectedEnemyJob][0].strength, monsters[selectedEnemyJob][0].vitality, monsters[selectedEnemyJob][0].magic, 0, 0)
console.log(player);
console.log(enemy)





//definition of variable

let action1 = document.querySelector("#action1");
let action2 = document.querySelector("#action2");
let action3 = document.querySelector("#action3");
let action4 = document.querySelector("#action4");

let canAttack = true;


action1.addEventListener('click', function () {
  tools.combatView(player,canAttack,enemy,1);
});

action2.addEventListener('click', function () {
  tools.combatView(player,canAttack,enemy,2);
});

action3.addEventListener('click', function () {
  tools.combatView(player,canAttack,enemy,3);
});

action4.addEventListener('click', function () {

  if (canAttack) {
    let bonus = tools.computeBonus(player)
    let skill = new Skill(player, 4);
    let dmg = -(skill.dealAllDmg(bonus));

    canAttack = false;
    tools.combatAction(player, enemy, dmg, true);
    setTimeout(() => {
      canAttack = true;
    }, 1300);

    //gestion barre de vie player
    let playerHealthBar = document.querySelector(`#player1 .bloc-health .health`);
    //console.log(` pdv actuel pre coup: ${player.health}`)
    player.manageHealth();


    console.log(` pdv actuel : ${player.health}`)

    if (player.health > 0) {
      playerHealthBar.textContent = `${Math.floor((player.health / player.maxHealth) * 100)}%`
      playerHealthBar.style.width = `${(player.health / player.maxHealth) * 100}%`
    }
    else {
      playerHealthBar.textContent = `0%`
      playerHealthBar.style.width = `0%`
      console.log('ded');
    }

    //gestion barre de vie enemy
    let enemyHealthBar = document.querySelector(`#enemy .bloc-health .health`);
    //console.log(` pdv actuel pre coup: ${enemy.health}`)
    enemy.takeDamage(dmg);
    enemy.manageHealth()


  }
});


//affichage des compétences
action1.textContent = player.ability1;
action2.textContent = player.ability2;
action3.textContent = player.ability3;
action4.textContent = player.ability4;

//gestion des noms de perso
let player1 = document.querySelector(`#player1>p`)
player1.textContent = player.name;

let opponent = document.querySelector(`#enemy>p`)
opponent.textContent = enemy.name;

//gestion du skin enemy

let enemyId = document.querySelector('.gameScreen>div')

if (enemy.profession === "Ghost") {
  enemyId.id = "Ghost";
}
else if (enemy.profession === "Mecabird") {
  enemyId.id = "Mecabird";
}
else if (enemy.profession === "Little Mage") {
  enemyId.id = "Little-Mage";
};