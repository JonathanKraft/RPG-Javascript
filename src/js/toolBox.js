import { Character } from './character';
import CharacterAttributs from '../json/jobAttributs.json'
import {Skill} from './skill'

export class Toolbox {
  constructor() {

  }
  createCharacter(name, profession) {

    if (profession === "Wizard") {
      let character = new Character(name, profession, 160, 200, 10, 10, 20);
      return character;
    }
    else if (profession === "Fighter") {
      let character = new Character(name, profession, 200, 100, 18, 12, 10);
      return character;
    }
    else if (profession === "Thief") {
      let character = new Character(name, profession, 180, 150, 20, 10, 10);
      return character;
    }
    else if (profession === "Paladin") {
      let character = new Character(name, profession, 190, 140, 13, 15, 12);
      return character;
    }
  }

  /**
 * 
 * @param {string} from the source of the damage
 * @param {string} to the char wounded
 * @param {number} dmg the number of damage
 */
  addLogCombat(from, to, dmg) {
    let logCombat = document.createElement('p');
    logCombat.textContent = `${to} takes ${dmg} damages from ${from}`;

    let logScroll = document.querySelector('#scroll');
    logScroll.prepend(logCombat);

    return dmg;
  }

  /**
 * 
 * @param {Character} character character object
 */
  computeBonus(character) {

    let randomizer = Math.random() * (1.5 - 1 + 1) + 1;


    if (character.profession === "Fighter") {

      let skill = Math.round(character.strength * randomizer);
      return skill / 10;
    }

    else if (character.profession === "Wizard") {

      let skill = Math.round(character.magic * randomizer);
      return skill / 10;
    }

    else if (character.profession === "Thief") {

      let skill = Math.round(character.strength + 5 * randomizer);
      return skill / 10;
    }

    else if (character.profession === "Paladin") {

      console.log(character.strength);

      let skill = Math.round((character.strength + character.magic) / 2 * randomizer)

      return (skill / 10);
    }

  }

  /**
 * Verifies if the fighters are alive. When one dies, his alive prop turn to false
 * @param {Character} player 
 * @param {Character} enemy 
 */
  isOver(enemy) {
    if (enemy.health <= 0) {
      enemy.alive = false;
      alert(`${enemy.name} is defeated. \n Congratulation!`);
      location.reload();
    }
  }

  /**
   * Launches a battle phase between two Characters. When the player does an action, the enemy replies after 1.2 seconds. Player can't play for 2 seconds
   * @param {Character} player 
   * @param {Character} enemy 
   * @param {Number} damage 
   */
  combatAction(player, enemy, damage = 0, heal = false) {
    let combatOver = false;
    //gestion barre de vie player
    let playerHealthBar = document.querySelector(`#player1 .bloc-health .health`);
    //gestion barre de vie enemy
    let enemyHealthBar = document.querySelector(`#enemy .bloc-health .health`);
    if (!heal) {
      enemy.health -= damage;
      enemy.manageHealth();
      this.addLogCombat(player.name, enemy.name, damage)

      if (enemy.health > 0) {
        enemyHealthBar.textContent = `${Math.floor((enemy.health / enemy.maxHealth) * 100)}%`
        enemyHealthBar.style.width = `${(enemy.health / enemy.maxHealth) * 100}%`
      }
      else {
        enemyHealthBar.textContent = `0%`
        enemyHealthBar.style.width = `0%`
        this.isOver(enemy);
      }

      console.log(`enemy health ${enemy.health} and ${damage} dmg`)
      if (enemy.health > 0) {
        setTimeout(() => {
          let enemyDamage = Math.round(enemy.magic * (Math.random() * (1.5 - 1 + 1) + 1));
          player.health -= enemyDamage ;

          player.manageHealth();

          if (player.health > 0) {
            playerHealthBar.textContent = `${Math.floor((player.health / player.maxHealth) * 100)}%`
            playerHealthBar.style.width = `${(player.health / player.maxHealth) * 100}%`
          }
          else {
            playerHealthBar.textContent = `0%`
            playerHealthBar.style.width = `0%`
            this.gameOver();
          }

          console.log(`Player health ${player.health} and ${enemy.magic}`)

          this.addLogCombat(enemy.name, player.name, enemyDamage)

        }, 1200);
      }
    }
    else {
      player.health += 22;
      player.manageHealth();
      //this.addLogCombat(this.addLogCombat(player.name, player.name, -22))

      if (player.health > 0) {
        playerHealthBar.textContent = `${Math.floor((player.health / player.maxHealth) * 100)}%`
        playerHealthBar.style.width = `${(player.health / player.maxHealth) * 100}%`
      }
      else {
        playerHealthBar.textContent = `0%`
        playerHealthBar.style.width = `0%`
        this.gameOver();
      }

      console.log(player.health);

      setTimeout(() => {
        enemy.health += 22;
        enemy.manageHealth();
        console.log(`enemy health ${enemy.health}`)

        if (enemy.health > 0) {
          enemyHealthBar.textContent = `${Math.floor((enemy.health / enemy.maxHealth) * 100)}%`
          enemyHealthBar.style.width = `${(enemy.health / enemy.maxHealth) * 100}%`
        }
        else {
          enemyHealthBar.textContent = `0%`
          enemyHealthBar.style.width = `0%`
          this.isOver(enemy);
        }

        this.addLogCombat(player.name, enemy.name, damage)

      }, 1200);
    }

  }

  gameOver() {
    if (confirm('You are dead. \n Click \"Ok\" to restart with this character.\n Click \"Annuler\" to creat a new character')) {
      location.reload();
    }
    else {
      location.href = "index.html";
    }
  }

  /**
 * 
 * @param {Character} player 
 * @param {boolean} canAttack 
 * @param {Character} enemy 
 * @param {number} buttonNumber 
 */
  combatView(player, canAttack, enemy, buttonNumber) {
    if (canAttack) {
      let skill = new Skill(player, buttonNumber);
      let bonus = this.computeBonus(player)
      let dmg = skill.dealAllDmg(bonus);
      

      console.log(`Max health player : ${player.maxHealth}`)

      canAttack = false;
      this.combatAction(player, enemy, dmg);
      console.log(dmg)
      setTimeout(() => {
        canAttack = true;
      }, 1300);
      //console.log(` pdv actuel : ${player.health} de ${player.name}`)
      

      //console.log(` pdv actuel pre coup: ${enemy.health} de ${enemy.name}`)
      enemy.takeDamage(dmg);
      enemy.manageHealth();

      //console.log(` pdv actuel : ${enemy.health} de ${enemy.name}`)

      
    };
  };
}